#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <iostream>

#include "AppBase.hpp"
#include <cstdlib>
#include <stdexcept>
#include <array>
#include <vector>
#include <algorithm>

int main() {


   /* glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    GLFWwindow* window = glfwCreateWindow(800, 600, "Vulkan window", nullptr, nullptr);

    while(!glfwWindowShouldClose(window)) {
        glfwPollEvents();
    }

    glfwDestroyWindow(window);
    glfwTerminate();*/
    std::array<int, 2> intArr {1,2};
    std::cout << "size of element is " << sizeof(intArr[0]) << std::endl;

    vke::AppBase app{};

    try
    {
        app.run();
    }
    catch(const std::exception& e)
    {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
    



    return 0;
}