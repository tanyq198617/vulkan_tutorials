
#pragma once

#include "AppWindow.hpp"
#include "AppSwapChain.hpp"
#include "RenderPipeline.hpp"
#include "AppDevice.hpp"
#include <memory>
#include <vector>

namespace vke
{

    class AppBase
    {

    public:
        static constexpr int WIDTH = 800;
        static constexpr int HEIGHT = 600;
        void run();
        AppBase();
        ~AppBase();
        AppBase(const AppBase &) = delete;
        AppBase &operator=(const AppBase &) = delete;


    private:
        AppWindow mAppWindow{WIDTH,HEIGHT,"HELLO VULKAN!"};
        AppDevice mDevice{mAppWindow};
        std::unique_ptr<RenderPipeline> mRenderPipeline;
        AppSwapChain mSwapChain{mDevice,mAppWindow.getExtent()};
        VkPipelineLayout mPipelineLayout;
        std::vector<VkCommandBuffer> mCommandBuffers;

        void createPipelineLayout();
        void createPipeline();
        void createCommandBuffers();
        void drawFrame();

   
    };

}