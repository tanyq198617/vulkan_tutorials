
#include "AppWindow.hpp"
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <array>

namespace vke
{

    AppWindow::AppWindow(int width, int height, std::string name) : mWidth{width}, mHeight{height}, mWindowName{name}
    {
        initWindow();
    }

    AppWindow::~AppWindow()
    {
        glfwDestroyWindow(mWindow);
        glfwTerminate();
    }

    void AppWindow::initWindow()
    {
        glfwInit();
        glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

        mWindow = glfwCreateWindow(mWidth, mHeight, mWindowName.c_str(), nullptr, nullptr);
    }

    bool AppWindow::shouldClose()
    {
        return glfwWindowShouldClose(mWindow);
    }

    void AppWindow::printVKExtensionInfo()
    {
        uint32_t extensionCount = 0;
        vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
        std::vector<VkExtensionProperties> extensions(extensionCount);
        vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());
        std::cout << "available extentions info:\n";
        for (const auto &extension : extensions)
        {
            std::cout << '\t' << extension.extensionName << '\n';
        }

        uint32_t requireExtensionCount = 0;
        glfwGetRequiredInstanceExtensions(&requireExtensionCount);
    }

    void AppWindow::testApi()
    {

        std::vector<const char *> deviceExtensions{VK_KHR_SWAPCHAIN_EXTENSION_NAME};
    }

    void AppWindow::createWindowSurface(VkInstance instance, VkSurfaceKHR *surface)
    {
        if (glfwCreateWindowSurface(instance, mWindow, nullptr, surface) != VK_SUCCESS)
        {
            throw std::runtime_error("failed to create window surface!");
        }
    }

}
