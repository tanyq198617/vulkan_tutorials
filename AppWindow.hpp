#pragma once

#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <string>

namespace vke
{

    class AppWindow
    {
    private:
        /* data */
        void initWindow();
        int mWidth;
        int mHeight;
        std::string mWindowName;
        GLFWwindow *mWindow;

    public:
        AppWindow(int width, int height, std::string name);
        ~AppWindow();

        AppWindow(const AppWindow &) = delete;
        AppWindow &operator=(const AppWindow &) = delete;
        void createWindowSurface(VkInstance instance, VkSurfaceKHR *surface);

        bool shouldClose();

        void printVKExtensionInfo();
        void testApi();

        VkExtent2D getExtent() {
            return {static_cast<uint32_t>(mWidth),static_cast<uint32_t>(mHeight)};
        }
    };

}
